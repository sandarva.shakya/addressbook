# ADDRESS BOOK

A simple addressbook using bash script. 📔

## Usage

Add, Edit, Delete and Search person by using the cli menu.

## Setting up Locally

- Create a folder in the project directory.
  ```
  mkdir "your_directory_name"
  ```
- Create a file inside that directory.
  ```
  touch "your_file_name"
  ```
- Change the variables DIR and FILENAME
- Run the script
  ```
  ./addressbook.sh
  ```
