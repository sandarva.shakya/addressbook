#!/bin/sh

# Global Variables
DIR=file
FILENAME=addressbook.txt

# Function to show menu
show_menu () {
    MENU_TEXT="--------------MENU------------\n
            1. Add Person\n
            2. Edit Person\n
            3. Search Person\n
            4. Remove Person\n
            5. Show Address Book\n
            6. Exit\n
    "
    echo $MENU_TEXT
    echo "Enter your option: "
}

# Funtion to add person
add_person () {
    mkdir -p $DIR
    cd "./${DIR}"
    echo "Enter full-name: "
    read PERSON_NAME
    echo "Enter number: "
    read NUMBER
    echo "Enter email: "
    read PERSON_EMAIL
    echo "Are you sure you want to add this person to your address book?[y for yes]: "
    read CONFIRM
    if [ "$CONFIRM" = "y" ]; then
        echo "Name [$PERSON_NAME]: Number [$NUMBER]: Email [$PERSON_EMAIL]" >> $FILENAME
        echo "Person Successfully Added to the address book"
    else
        echo "Person not added to the address book"
    fi
    cd ..
}

# Funtion to edit the person
edit_person () {
    if [ -e "./${DIR}/${FILENAME}" ];
    then
        echo "Enter the name of the person you want to edit: "
        read EDIT_SEARCH
        if grep "$EDIT_SEARCH" "./${DIR}/${FILENAME}";
        then
            echo "What do you want to edit?[Name | Number | Email]: "
            read EDIT
            if [ "$EDIT" = "Number" ];
            then
                edit_number
            elif [ "$EDIT" = "Name" ];
            then
                edit_name
            elif [ "$EDIT" = "Email" ];
            then
                edit_email
            else
                echo "$EDIT does note exist as a field"
            fi
        else
            echo "The name does not exist in the addressbook"
        fi
    else
        echo "You have not created an addressbook!!"
    fi
}

# Edits the number field in the file
edit_number () {
    echo "Enter the correct number: "
    read NUMBER
    sed "s/Number \[[^]]*\]/Number \[$NUMBER\]/g" "./${DIR}/${FILENAME}"
    echo "Are you sure this is the correct one?[y for yes]: "
    read CONFIRM 
    if [ "$CONFIRM" = "y" ]; then
        sed -i "s/Number \[[^]]*\]/Number \[$NUMBER\]/g" "./${DIR}/${FILENAME}"
        echo "Person Successfully Edited"
    else
        echo "Person not edited"
    fi
}

# Edits the name in the file
edit_name () {
    echo "Enter the correct name: "
    read NAME
    sed "s/Name \[[^]]*\]/Name \[$NAME\]/g" "./${DIR}/${FILENAME}"
    echo "Are you sure this is the correct one?[y for yes]: "
    read CONFIRM 
    if [ "$CONFIRM" = "y" ]; then
        sed -i "s/Name \[[^]]*\]/Name \[$NAME\]/g" "./${DIR}/${FILENAME}"
        echo "Person Successfully Edited"
    else
        echo "Person not edited"
    fi
}

# Edits the name in the file
edit_email () {
    echo "Enter the correct email: "
    read EMAIL
    sed "s/Email \[[^]]*\]/Email \[$EMAIL\]/g" "./${DIR}/${FILENAME}"
    echo "Are you sure this is the correct one?[y for yes]: "
    read CONFIRM 
    if [ "$CONFIRM" = "y" ]; then
        sed -i "s/Email \[[^]]*\]/Email \[$EMAIL\]/g" "./${DIR}/${FILENAME}"
        echo "Person Successfully Edited"
    else
        echo "Person not edited"
    fi
}

# Funtion to search a specific person by name
search_person () {
    if [ -e "./${DIR}/${FILENAME}" ];
    then    
        echo "Enter the name of the person you want to search: "
        read SEARCH
        if grep "$SEARCH" "./${DIR}/${FILENAME}";
        then
            echo "Details of the person: \n"
            echo "$(grep $SEARCH "./${DIR}/${FILENAME}")"
        else 
            echo "The name does not exist in the addressbook"
        fi
    else
        echo "You have not created an addressbook!!"
    fi
}

# Funtion to remove person
remove_person () {
    if [ -e "./${DIR}/${FILENAME}" ];
    then
        echo "Enter the name of person to remove: "
        read NAME
        if [ -e "./${DIR}/${FILENAME}" ];
        then
            echo "Are you sure you want to add this person to your address book?[y for yes]: "
            read CONFIRM
            if [ "$CONFIRM" = "y" ]; then
                sed -i "/${NAME}/d" "./${DIR}/${FILENAME}"
            else
                echo "Person not deleted"
            fi
        else
            echo "You have not created an addressbook!!"
        fi
    else
        echo "You have not created an addressbook!!"
    fi
}

# Funtion to completely show the address book
show_address_book () {
    if [ -e "./${DIR}/${FILENAME}" ];
    then
        cat "./${DIR}/${FILENAME}"
    else
        echo "You have not created an addressbook!!"
    fi
}

# Main Funtion
main () {
    while :
    do
        show_menu
        read OPTION
        if [ "$OPTION" = "1" ];
        then
            add_person
        elif [ "$OPTION" = "2" ];
        then
            edit_person
        elif [ "$OPTION" = "3" ]
        then
            search_person
        elif [ "$OPTION" = "4" ];
        then
            remove_person
        elif [ "$OPTION" = "5" ];
        then
            show_address_book
        elif [ "$OPTION" = "6" ];
        then
            echo "Thank you for using addressbook. Have a nice day!!"
            break
        else
            echo "Invalid Option!!"
        fi
    done
}

# Main function call
main